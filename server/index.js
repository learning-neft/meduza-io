// var http = require("http");

const axios = require("axios"),
  url = require("url"),
  express = require("express"),
  cors = require("cors"),
  app = express();
app.use(cors());

app.get("/", function(req, res) {
  let options = {
    host: "meduza.io",
    protocol: "https",
    port: 80,
    path: "/api/v3/search"
  };
  let p = url.parse(req.url);
  options.path += p.search;
  axios
    .get(options.protocol + "://" + options.host + options.path)
    .then(function(response) {
      res.json(response.data);
    })
    .catch(function(error) {
      res.json({
        data: {}
      });
    });
});
app.listen(3000, function() {
  console.log("Listen 3000");
});
