"use strict";

const app = new Vue({
  el: "#meduza",
  data: {
    chrono: "news",
    locale: "ru",
    news: [],
    openNews: null,
    page: 1,
    limit: 24,
    total: 0
  },
  mounted: function() {
    this.getArticles();
  },
  methods: {
    setPage: function(page) {
      this.page = page;
      this.getArticles();
    },
    setChrono: function(chrono) {
      this.page = 1;
      this.chrono = chrono;
      this.getArticles();
    },
    getArticles: function() {
      const self = this,
        url = new URL("http://localhost:3000");

      self.news = [];

      url.searchParams.append("chrono", self.chrono);
      url.searchParams.append("locale", self.locale);
      url.searchParams.append("page", self.page - 1);
      url.searchParams.append("per_page", self.limit);
      console.log(url);
      fetch(url)
        .then(function(response) {
          if (!response.ok) {
            throw response;
          }
          return response.json();
        })
        .then(function(json) {
          if (typeof json.documents != "undefined" && json.documents) {
            try {
              for (let key in json.documents) {
                let item = json.documents[key],
                  insert = {
                    title: item.title,
                    url: item.url,
                    date: item.pub_date,
                    image: null
                  };
                if (
                  item.image
                  //&& item.image.display != "none"
                ) {
                  insert.image = "https://meduza.io" + item.image.small_url;
                }
                self.news.push(insert);
              }
              console.log(self.news);
            } catch (e) {}
          }
        })
        .catch(function(err) {
          console.log(err);
        });
    }
  }
});
