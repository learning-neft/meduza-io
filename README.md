# Запуск проекта

- Склонируйте репозиторий с помощью комманды `https://gitlab.com/learning-neft/meduza-io`
- Перейдите в папка meduza-io (в терминале `cd meduza-io`)
- Выполните комманду `npm install`
- Передите в папку **server** и выполните `npm install`
- В папке **server** выполните комманду `node index.js` для запуска сервера
